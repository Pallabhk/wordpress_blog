<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'blog_word');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'mrmx0k;x]Ce]U^q$[{Q&2neTQpnDSmP$2x~^Yb?vrAY@^9z@D?>=7XN{utT2z-IG');
define('SECURE_AUTH_KEY',  '}(eAeNb00Pi_tlC_+Hg*Oj3s}V[VFAy/bsC[d6qK_}<xAmq G//egrG{qYuNDg+R');
define('LOGGED_IN_KEY',    '4u/v3py>iM1O{S$shr0i87vSeH&]@$qL_YZ>>lG2W//]Oxff_,y]A X@#Sx|p436');
define('NONCE_KEY',        'Z$h~}ZF:BNe*;_o,n s$uIki^9=WrkmP#+>+Z}lgS2Di757l|w&~-e2~|kv|!f4!');
define('AUTH_SALT',        '5WV(m/v?@neT@>j3HP_FN:Dlu0kNHKzvh`,=P/FC,/?y??+BY}D;L%dD9RJthx~:');
define('SECURE_AUTH_SALT', '@UyF0Qu%+JnS;ywU(OH6H=5p)5EPLquTZpEML^r1fNB|Z>3-[v=?nlI+v$!TYr<Q');
define('LOGGED_IN_SALT',   '~`YCkuYVbs[n</M9/Ng^r&ncq!n1/y7-!-u)rC~9r>ltxs?vA@A/+sU8hT-G D6.');
define('NONCE_SALT',       '49^T8rR. H<S[_ELj&JI12!mGpbfT~mLjnAX1rRQ6?x?yb0=`k%a4@(&D!fX.7)}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
