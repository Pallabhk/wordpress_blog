<?php
    function office_master_theme_support(){
       add_theme_support('title-tag');
        add_image_size('slider-img', 1500, 500, true);
        add_theme_support('post-thumbnails');
        register_nav_menus(array(
            'primary_menu'=>'Primary Menu'
        ));
    }
add_action('after_setup_theme','office_master_theme_support');

function office_master_css_js(){

    wp_enqueue_style('google-fonts-1','//fonts.googleapis.com/css?family=Open+Sans:400,300');
    wp_enqueue_style('google-fonts-2','//fonts.googleapis.com/css?family=PT+Sans');
    wp_enqueue_style('google-fonts-3','//fonts.googleapis.com/css?family=Raleway');
    wp_enqueue_style('bootstrap',get_template_directory_uri().'/assets/bootstrap/css/bootstrap.css');
    wp_enqueue_style('font-awesome',get_template_directory_uri().'/assets/css/font-awesome.min.css');
    wp_enqueue_style('main-css',get_template_directory_uri().'/assets/css/style.css');
    wp_enqueue_style('animate-css',get_template_directory_uri().'/assets/css/animate.min.css');
    wp_enqueue_style('main-stylesheet-css',get_stylesheet_uri());

    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap-min-js',get_template_directory_uri().'/assets/bootstrap/js/bootstrap.min.js','jquery','null','false');
    wp_enqueue_script('wow-min-js',get_template_directory_uri().'/js/wow.min.js','jquery','null','false');

}
add_action('wp_enqueue_scripts','office_master_css_js');
function footer_extra_script(){ ?>
    <script>
    new WOW().init();
    </script>
<?php }
add_action('wp_footer','footer_extra_script',30);


function office_master_fallback_menu(){?>
    <ul class="nav navbar-nav pull-right">
        <li class="active">
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">About</a>
        </li>
        <li>
            <a href="#">Blog</a>
        </li>
        <li>
            <a href="#">Team</a>
        </li>
        <li>
            <a href="#"><span>Contact</span></a>
        </li>
    </ul>
<?php }

function office_master_custom_post(){
    register_post_type('Slider',array(
       'labels' => array(
           'name'        => 'slider Menu',
           'menu_name'   => 'Slider Menu',
           'all_items'   =>'all Slider items',
           'add_new'     =>'Add new slider',
           'add_new_item'=>'Add new slider items'
       ),
        'public' =>true,
        'supports'=>array(
            'title','thumbnail','revisions','custom-fields','page-attributes'
        )
    ));

    register_post_type('services',array(
        'labels' => array(
            'name'        => 'services ',
            'menu_name'   => 'services Menu',
            'all_items'   =>'all services items',
            'add_new'     =>'Add new services',
            'add_new_item'=>'Add new services items'
        ),
        'public' =>true,
        'supports'=>array(
            'title','revisions','custom-fields','page-attributes'
        )
    ));
 }
add_action('init','office_master_custom_post');
include_once('inc/cmb-2-custom-field.php');
?>