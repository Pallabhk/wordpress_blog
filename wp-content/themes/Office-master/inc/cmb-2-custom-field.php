<?php
/**
 * Get the bootstrap!
 */
if ( file_exists( __DIR__ . '/cmb2/init.php' ) ) {
    require_once __DIR__ . '/cmb2/init.php';
} elseif ( file_exists(  __DIR__ . '/CMB2/init.php' ) ) {
    require_once __DIR__ . '/CMB2/init.php';
}

add_action( 'cmb2_admin_init', 'office_master_cmb2' );

function office_master_cmb2(){
    $pref ='_office-master_';
    $service_item = new_cmb2_box( array(
        'id'            => 'service_metabox',
        'title'         => __( 'service Metabox', 'office_master' ),
        'object_types'  => array( 'services' ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left

    ) );
    $service_item->add_field( array(
        'name'       => __( 'Service Icon', 'office_master' ),
        'desc'       => __( 'Add service icon here', 'office_master' ),
        'id'         => $pref. 'services_icon',
        'repeatable'  =>true,
        'type'       => 'text',
    ) );
    $service_item->add_field( array(
        'name'       => __( 'Service Discription', 'office_master' ),
        'desc'       => __( 'Add service Discription here', 'office_master' ),
        'id'         => $pref. 'service_discription',
        'type'       => 'textarea',
    ) );
    $service_item->add_field( array(
        'name'       => __( 'Service link url', 'office_master' ),
        'desc'       => __( 'Add service link url here', 'office_master' ),
        'id'         => $pref. 'service_link_url',
        'type'       => 'text',
    ) );
    $service_item->add_field( array(
        'name'       => __( 'Service link title', 'office_master' ),
        'desc'       => __( 'Add service link title here', 'office_master' ),
        'id'         => $pref. 'service_link_title',
        'type'       => 'text',
    ) );
    $service_item->add_field( array(
        'name'       => __( 'Service animation', 'office_master' ),
        'desc'       => __( 'Add service animation here', 'office_master' ),
        'id'         => $pref. 'service_animation',
        'type'       => 'text',
    ) );
    $slider_item = new_cmb2_box( array(
        'id'            => 'slider_metabox',
        'title'         => __( 'slider Metabox', 'office_master' ),
        'object_types'  => array( 'slider','page' ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left

    ) );
    $slider_item->add_field( array(
        'name'       => __( 'Slider caption', 'office_master' ),
        'desc'       => __( 'Add service Slider caption here', 'office_master' ),
        'id'         => $pref. 'slider_caption',
        'type'       => 'text',
    ) );

    $special_item= new_cmb2_box( array(
        'id'            => 'special_metabox',
        'title'         => __( 'special Metabox', 'office_master' ),
        'object_types'  => array( 'slider','page' ), // Post type
        'show_on'       =>array(
            'key'      =>'id',
            'value'    =>'9'
        ),
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left

    ) );
    $special_item->add_field( array(
        'name'       => __( 'special caption', 'office_master' ),
        'desc'       => __( 'Add service Slider caption here', 'office_master' ),
        'id'         => $pref. 'slider_caption',
        'type'       => 'text',
    ) );
}